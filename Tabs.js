/* global define*/
define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-class',
  './SiteController',
], (declare, domConstruct, domClass, SiteController) =>

  declare(SiteController, {
    domNode: null,
    _initiallized: false,
    _name2TabNode: null,
    _currentTabName: null,

    //= ==================================================
    // Inherited methods
    //= ==================================================
    constructor(params, node) {
      this.domNode = node;
      domClass.add(this.domNode, 'spaTabControl');
      this._name2TabNode = {};
    },
    /**
     * @param viewName
     * @param params
     */
    show(viewName) {
      this.inherited('show', arguments);
      if (!this._initiallized) {
        this._initiallized = true;
        // TODO the right parent view, not just 0
        const views = this.site.getConfig().views[0].paths;
        if (views) {
          views.forEach((viewDef) => {
            this._name2TabNode[viewDef.path] = domConstruct.create('a', {
              class: `${viewDef.name} spaTab`,
              innerHTML: viewDef.name,
              href: `http://local.entryscape.com:8080/spa/samples/${viewDef.path}`,
            }, this.domNode);
          }, this);
        }
      }
      if (this._currentTabName != null) {
        domClass.remove(this._name2TabNode[this._currentTabName], 'selected');
        delete this._currentTabName;
      }
      const node = this._name2TabNode[viewName];
      if (node != null) {
        domClass.add(node, 'selected');
        this._currentTabName = viewName;
      }
      preventClick();
    },
  }));
