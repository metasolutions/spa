/* global define*/
define([
  'require',
  'dojo/ready',
  'dojo/_base/lang',
], (require, ready, lang) => (siteConf, callback) => {
  const deps = [siteConf.siteClass, siteConf.routerClass, siteConf.handlerClass, 'dojo/domReady!'];

  /**
   * Create the site object
   */
  require(deps, (SiteClass, RouterClass, HandlerClass) => {
    const site = new SiteClass({
      config: siteConf,
      router: RouterClass,
      handler: HandlerClass,
      queryParams: window.queryParams || '',
    });
    if (callback) callback(site);

    window.preventClick = () => {
      const clickHandler = lang.hitch(site._handler, site._handler.clickHandler);
      document.addEventListener('click', clickHandler);
    };

    window.whenViewLoaded = (callBack) => {
      const t = setInterval(() => {
        if (window.__spaViewLoaded === true) {
          clearInterval(t);
          callBack();
        }
      }, 50);
    };

    whenViewLoaded(preventClick);

    setTimeout(() => {
      if (site.initialize) site.initialize();
    }, 1);
  });
});
