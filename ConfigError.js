define([], () =>
  /**
   * Just a wrapper of Error
   */
  class ConfigError extends Error {
    constructor(...params) {
      super(...params);

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, ConfigError);
      }
    }
  });
