/* global define*/
define([
  'dojo/_base/declare',
  'spa/Site',
  'dojo/query',
  'dojo/dom-attr',
  'dojo/dom-style',
], (declare, Site, query, domAttr, domStyle) => declare(Site, {
  constructor() {
    this.initialize();
  },
  initialize() {
            // Create views from DOM and provide nodes for each.
    const config = this.getConfig();
    query('.spaSite .spaControl').forEach((node) => {
      config.controlNode = node;
    });
    config.views = []; // Really throw away views from config?
    query('.spaSite .spaView').forEach((node) => {
      const classes = domAttr.get(node, 'class').split(/\s+/);
      config.views.push({ name: classes[1], node });
      domStyle.set(node, 'display', 'none');
    }, this);

    this.inherited('initialize', arguments);
  },
}));
