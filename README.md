# Single Page Application (SPA) library

The purpose of this javascript library is to simplify the way you build single page applications.
When you use the SPA library you provide a configuration which defines the single page application as a site with
a range of views. The configuration may also include a SiteController, often in the form of tabs or breadcrumbs.

## Getting started
First you need to install the dependencies:

    cd path_to_spa
    bower install

For this to work you need to have [bower](http://bower.io/) installed which in turn depends on
[Node](http://nodejs.org/) and [npm](https://www.npmjs.org/).

Now you should be able to try out the various examples in the ```samples``` directory.

(You need to load them via http://, not file:/// for the dependancies to load correctly due to the limitations of
how modern browsers handles the file URI schemes.)

## Note on support for browser history
The SPA library supports switching between views in a manner which does not trigger a page reload while still
being recorded into the browser history. That is, the back and forward buttons in the browser works as expected.
It is also possible to provide regular links from inside one view to another view.
The same mechanism also makes it possible to pass along parameters to a view.
This means that you can store state changes within a view into the browser history for later navigation.
Currently the change of view and state is reflected as part of the fragment identifier in the url.

## The site configuration
The site configuration is a json structure where each view is defined. Consider the following simple configuration
wich is a simplication of the config in ```samples/example1.html```:

    {
        siteClass: "spa/Site",
        controlClass: "spa/Tabs",
        startView: "view1",
        views: [
                    {"name": "view1", "class": "dijit/layout/ContentPane",
                        "constructorParams": {content: "<p>Some text1</p>"}},
                    {"name": "view2", "class": "dijit/layout/ContentPane",
                        "constructorParams": {content: "<p>Some text2</p>"}},
               ]
    }

As you see in this example each view has a name, a class to instantiate for this view, and potentially some constructor parameters.
It is also possible to detect parts of the configuration directly from the html, see ```samples/example2.html```
for how this is done. Note that in this case a specific subclass of Site is responsible for the detection of the config,
e.g. the InlineSite class.

## Types of views
In the example above the ```dijit/layout/ContentPane``` was used to instantiate the view. This is a predefined UI-component
provided by the Dojo toolkit that is good at showing regular HTML. In the example above the HTML is provided as a constructor parameter.
In the inline case no view class was provided at all and behind the scenes the library uses a default view that
does nothing at all except wrap a dom-node.
In ```samples/example3.html``` we see three more common examples of views.
1. Pointing to an external html file, using the ```ContentPane``` again but this with the href constructor parameter.
(Note that this only works with files from the same domain.)
2. Defining a specific class that programatically generates HTML and support for interaction.
3. Defining a specific class that relies on a HTML template for generating HTML and support for interaction.

Note that in a more realistic setting the specific classes should probably be located in their own files and loaded as AMD modules.
It is only for convenience that they are provided inline in ```samples/example3.html```.

## The view hierarchy
The configuration may specify that the views are organized into a hierarchy. Such a hierarchy can be used in various ways,
for instance, the ```Breadcrumbs``` siteController is a good example that simply displays the views in a breadcrumb list
based on their position in the hierarchy.

    hierarchies: [{
                    "view": "view1",
                    "subViews": [
                        {"view": "view1.1",
                         "subViews": ["view1.1.1"]},
                        "view1.2"
                    ]}, {
                    "view": "view2"
                }]

See ```samples/example4.html``` for a complete example.