/* global define*/
define([
  'dojo/_base/declare',
  'dojo/_base/array',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-class',
  './SiteController',
], (declare, array, domConstruct, domAttr, domClass, SiteController) => declare(SiteController, {

  domNode: null,
  _initiallized: false,

  //= ==================================================
  // Inherited methods
  //= ==================================================
  constructor(params, node) {
    this.domNode = node;
    domClass.add(this.domNode, 'spaBreadcrumbControl');
  },

  showHierarchy(viewName, params, hierarchy) {
    domAttr.set(this.domNode, 'innerHTML', '');

    hierarchy.forEach((view, idx) => {
      if (idx > 0) {
        domConstruct.create('span', {
          class: 'spaCrumbSeparator',
        }, this.domNode);
      }
      if (view === viewName) {
        const node = domConstruct.create('span', {
          class: `${view} spaCrumb selected`,
        }, this.domNode);
        this.setViewDisplayHTML(node, view, params, true);
      } else {
        const node = domConstruct.create('a', {
          class: `${view} spaCrumb`,
          href: this.site.getViewPath(view),
        }, this.domNode);
        this.setViewDisplayHTML(node, view, params, false);
      }
    }, this);
  },

  /**
   * @param node
   * @param view
   * @param params
   * @param isSelected
   */
  setViewDisplayHTML(node, view) {
    domAttr.set(node, 'innerHTML', view);
  },
}));
