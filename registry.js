define([], () => {
  const _registry = {};
  return {
    get(key) {
      return _registry[key];
    },
    set(key, value) {
      _registry[key] = value;
    },
  };
});
